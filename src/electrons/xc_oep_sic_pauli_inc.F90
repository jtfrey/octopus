!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

! ---------------------------------------------------------
!>@brief This routine calculates the SIC functional for the spinor case.
subroutine oep_sic_pauli(xcs, gr, psolver, namespace, space, rcell_volume, st, kpoints, oep, ex, ec)
  type(xc_t),          intent(inout) :: xcs
  type(grid_t),        intent(in)    :: gr
  type(poisson_t),     intent(in)    :: psolver
  type(namespace_t),   intent(in)    :: namespace
  type(space_t),       intent(in)    :: space
  FLOAT,               intent(in)    :: rcell_volume
  type(states_elec_t), intent(inout) :: st
  type(kpoints_t),     intent(in)    :: kpoints
  type(xc_oep_t),      intent(inout) :: oep
  FLOAT,               intent(inout) :: ex, ec

  integer  :: ip, ist, ispin
  FLOAT :: ex_sic, ec_sic, ex_, ec_
  FLOAT, allocatable :: vxc(:, :), vh_sic(:), density(:,:)
  CMPLX, allocatable :: psi(:, :)
  CMPLX :: term
  type(profile_t), save :: prof

  call profiling_in(prof, "XC_SIC_PAULI")
  PUSH_SUB(oep_sic_pauli)

  ASSERT(st%d%dim > 1)
  ASSERT(st%nik == 1)
  ASSERT(st%d%ispin == SPINORS)

  if (st%d%ispin == SPINORS .and. bitand(xcs%family, XC_FAMILY_LDA) == 0) then
    call messages_not_implemented('PZ-SIC with non-collinear spin and not LDA functional', namespace=namespace)
  end if

  SAFE_ALLOCATE(psi(1:gr%np, 1:st%d%dim))
  SAFE_ALLOCATE(density(1:gr%np, 1:4))
  SAFE_ALLOCATE(vxc(1:gr%np, 1:4))
  SAFE_ALLOCATE(vh_sic(1:gr%np))

  ! loop over states
  ex_ = M_ZERO
  ec_ = M_ZERO
  do ist = st%st_start, st%st_end
    if (abs(st%occ(ist, 1)) <= M_MIN_OCC) cycle ! we only need the occupied states

    vxc = M_ZERO
    call states_elec_get_state(st, gr, ist, 1, psi)

    ! get orbital density
    !$omp parallel do private(term)
    do ip = 1, gr%np
      density(ip, 1) = TOFLOAT(psi(ip, 1)*conjg(psi(ip, 1)))
      density(ip, 2) = TOFLOAT(psi(ip, 2)*conjg(psi(ip, 2)))
      term = psi(ip, 1)*conjg(psi(ip, 2))
      density(ip, 3) = TOFLOAT(term)
      density(ip, 4) = aimag(term)
    end do

    ex_sic = M_ZERO
    ec_sic = M_ZERO
    call xc_get_vxc(gr, xcs, st, kpoints, psolver, namespace, space, &
      density, SPINORS, rcell_volume, vxc, ex = ex_sic, ec = ec_sic)

    ex_ = ex_ - ex_sic * st%occ(ist, 1)
    ec_ = ec_ - ec_sic * st%occ(ist, 1)

    ! We now substract the Hartree self-interaction
    call lalg_axpy(gr%np, M_ONE, density(:, 2), density(:, 1))
    vh_sic = M_ZERO
    call dpoisson_solve(psolver, namespace, vh_sic, density(:, 1), all_nodes=.false.)
    do ispin = 1, 2
      call lalg_axpy(gr%np, M_ONE, vh_sic, vxc(:, ispin))
    end do
    ! Compute the corresponding energy contribution
    ex_ = ex_ - M_HALF*dmf_dotp(gr, density(:,1), vh_sic) * st%occ(ist, 1)

    !$omp parallel do
    do ip = 1, gr%np
      oep%zlxc(ip, 1, ist, 1) = oep%zlxc(ip, 1, ist, 1) - vxc(ip, 1) * conjg(psi(ip, 1)) &
        - cmplx(vxc(ip, 3),-vxc(ip, 4), REAL_PRECISION) * conjg(psi(ip, 2))
      oep%zlxc(ip, 2, ist, 1) = oep%zlxc(ip, 2, ist, 1) - vxc(ip, 2) * conjg(psi(ip, 2)) &
        - cmplx(vxc(ip, 3), vxc(ip, 4), REAL_PRECISION) * conjg(psi(ip, 1))
    end do
  end do

  if (st%parallel_in_states) then
    call comm_allreduce(st%mpi_grp, ec_)
    call comm_allreduce(st%mpi_grp, ex_)
  end if

  ec = ec + ec_
  ex = ex + ex_

  SAFE_DEALLOCATE_A(vxc)
  SAFE_DEALLOCATE_A(vh_sic)
  SAFE_DEALLOCATE_A(psi)
  SAFE_DEALLOCATE_A(density)

  POP_SUB(oep_sic_pauli)
  call profiling_out(prof)
end subroutine oep_sic_pauli


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
