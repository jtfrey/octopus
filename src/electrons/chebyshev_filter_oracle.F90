!! Copyright (C) 2023. A. Buccheri
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
#include "global.h"

module chebyshev_filter_oracle_oct_m
  use chebyshev_filter_bounds_oct_m
  use comm_oct_m
  use debug_oct_m
  use global_oct_m
  use, intrinsic :: iso_fortran_env
  use messages_oct_m
  use namespace_oct_m
  use profiling_oct_m
  use states_elec_oct_m
  use states_elec_calc_oct_m
  implicit none
  private

  !> Exposed routines
  public :: optimal_chebyshev_polynomial_degree

  !> @class Helper class of work arrays for a three-term recursion relation
  !! @brief Helper class of work arrays for a three-term recursion relation
  type three_term_relation
    FLOAT, allocatable :: n_minus_1(:)
    FLOAT, allocatable :: n(:)
    FLOAT, allocatable :: n_plus_1(:)
  contains
    procedure :: init => three_term_relation_init   !< @copydoc chebyshev_filter_oracle_oct_m::three_term_relation_init
    final :: finalize                               !< @copydoc chebyshev_filter_oracle_oct_m::finalize
  end type three_term_relation

contains

  !> @brief Initialize an instance `three_term_relation`
  subroutine three_term_relation_init(this, n)
    class(three_term_relation) :: this
    integer, intent(in) :: n

    PUSH_SUB(three_term_relation_init)
    SAFE_ALLOCATE(this%n_minus_1(1:n))
    SAFE_ALLOCATE(this%n(1:n))
    SAFE_ALLOCATE(this%n_plus_1(1:n))
    POP_SUB(three_term_relation_init)

  end subroutine three_term_relation_init

  !> @brief Finalize an instance `three_term_relation`
  subroutine finalize(this)
    type(three_term_relation), intent(inout) :: this

    PUSH_SUB(finalize)
    SAFE_DEALLOCATE_A(this%n_minus_1)
    SAFE_DEALLOCATE_A(this%n)
    SAFE_DEALLOCATE_A(this%n_plus_1)
    POP_SUB(finalize)

  end subroutine finalize

  !> @brief Approximate an optimal Chebyshev polynomial filter degree. Batchified.
  !!
  !! Batch/block wrapper for approximating an optimal Chebyshev polynomial filter degree, based on how much the magnitude of
  !! the Chebyshev polynomial scales the corresponding residuals. See @ref optimal_cheby_poly_max_order for details.
  subroutine optimal_chebyshev_polynomial_degree(namespace, st, ik, residual, residual_tol, bounds, max_degree, max_degree_estimate)
    type(namespace_t),               intent(in)       :: namespace              !< Namespace
    type(states_elec_t),             intent(in)       :: st                     !< KS states containing eigenvalues
    integer,                         intent(in)       :: ik                     !< k-point index
    FLOAT,                           intent(in)       :: residual(:)            !< Residual vector from prior SCF iteration
    FLOAT,                           intent(in)       :: residual_tol           !< Threshold at which the residuals are sufficiently small
    type(chebyshev_filter_bounds_t), intent(in)       :: bounds                 !< Polynomial filter bounds.
    integer,                         intent(in)       :: max_degree             !< Maximum degree to consider.
    integer,                         intent(out)      :: max_degree_estimate(:) !< Maximum polynomial degree estimate, per block (per k-point)

    type(three_term_relation)                         :: T                      !< Polynomal filter work arrays
    integer                                           :: minst, maxst           !< Start and end indices for states of a block
    integer                                           :: iblock

    integer :: max_buffer_len, block_plus_space, max_blocks_to_buffer           !< Message buffer sizes

    PUSH_SUB(optimal_chebyshev_polynomial_degree)

    ASSERT(size(max_degree_estimate) == st%group%nblocks)
    ASSERT(max_degree > 0)

    ! Allocate with an upper bound
    call T%init(st%nst)

    do iblock = st%group%block_start, st%group%block_end
      minst = states_elec_block_min(st, iblock)
      maxst = states_elec_block_max(st, iblock)
      max_degree_estimate(iblock) = optimal_polynomial_degree(residual(minst:maxst), st%eigenval(minst:maxst, ik), &
        bounds, T, residual_tol, max_degree)
    enddo

    if(debug%info) then
      call comm_allreduce(st%mpi_grp, max_degree_estimate)
      write(message(1), '(A, I6, X)') &
        'Debug: k-index, chebyshev filter degree estimate per batch', ik

      ! Catch overflow if nbatches exceeds total line buffer
      ! In this instance, the output will be truncated w.r.t. batches and the developer
      ! is not informed
      max_buffer_len = 256
      block_plus_space = 3
      max_blocks_to_buffer = int( (max_buffer_len - len(trim(message(1)))) / block_plus_space )

      do iblock = 1, min(st%group%nblocks, max_blocks_to_buffer)
        write(message(1), '(A, X, I2)') trim(message(1)), max_degree_estimate(iblock)
      enddo
      call messages_info(1, namespace=namespace)
    endif

    POP_SUB(optimal_chebyshev_polynomial_degree)

  end subroutine optimal_chebyshev_polynomial_degree

  !> @brief Approximate an optimal Chebyshev polynomial filter degree.
  !!
  !! Approximate an optimal Chebyshev polynomial filter degree based on how much the magnitude of the Chebyshev polynomial,
  !! evaluated for a set of eignevalues, scales the corresponding residuals. Based on the ABINIT implementation documented in
  !! [Parallel eigensolvers in plane-wave Density Functional Theory](https://doi.org/10.1016/j.cpc.2014.10.015).
  !!
  !! ## Selection Criteria
  !! The criteria are implemented such that for early SCF cycles, when the absolute residual values are large,
  !! the degree returned should not result in a reduction in the smallest residual by more than an order of magnitude.
  !! Once the absolute values of residuals are the same order of magnitude as `target_residual`, the absolute
  !! criterion should take over.
  !!
  !! \f$\epsilon * |T_n(E_1)| \ll 1\f$ is implemented to prevent obtaining large polynomial degrees, as this will result in the
  !! Gram matrix being ill-conditioned. Unlike Abinit, we apply this condition to the first eigenvalue of every block/batch
  !! rather than only the lowest eigenvalue, as it simplifies the implementation. In general, we do not expect this
  !! criterion to be evaluated unless \f$|T_n|\f$ increases exponentially from \f$n\f$ to \f$n+1\f$.
  !!
  !! If max_degree == 1, filter_degree = 1 is returned.
  !!
  !! @note
  !! * This function implements the simple Chebyshev polynomial filter, consistent with Algorithm 3.1 in
  !!   [Chebyshev-filtered subspace iteration method free of sparse diagonalization for solving the Kohn–Sham equation](https://doi.org/10.1016/j.jcp.2014.06.056)
  !!   The scaled polynomial was tested but the magnitude of the polynomial did not appear to consistently increase as a function
  !!   of degree. That implementation was assumed to be erroneous and removed, but should be revisited.
  !! * The Chebyshev polynomial increases in magnitude as the degree is increased, but the sign switches per iteration.
  !!   One therefore should implement \f$|T_n|\f$, which is a subtle detail missing from the ABINIT paper.
  function optimal_polynomial_degree(prior_residual, eigenvalues, bounds, T, target_residual, max_degree) result(filter_degree)
    FLOAT,                           intent(in)    :: prior_residual(:)    !< Residual vector from prior SCF iteration.
    FLOAT,                           intent(in)    :: eigenvalues(:)       !< Eigenvalues (or a subset) of eigenvalues from the prior SCF iteration.
    type(chebyshev_filter_bounds_t), intent(in)    :: bounds               !< Polynomial filter bounds.
    type(three_term_relation),       intent(inout) :: T                    !< Filter work arrays: Polynomial filter for the 3-term recursion relation.
    FLOAT,                           intent(in)    :: target_residual      !< Absolute value for target residual.
    integer,                         intent(in)    :: max_degree           !< Maximum polynomial degree to consider.
    integer                                        :: filter_degree        !< Optimal maximum polynomial order.

    FLOAT, allocatable                             :: shifted_eigenvalues(:)
    FLOAT                                          :: c, hw                !< Affine map values
    integer                                        :: n                    !< Filter order loop index
    integer                                        :: n_states             !< Number of states to operate on

    PUSH_SUB(optimal_polynomial_degree)
    ASSERT(max_degree > 0)

    ! Number of states in a group
    n_states = size(prior_residual)

    ! Consistent array lengths
    ASSERT(size(eigenvalues, 1) == n_states)

    c = bounds%center()
    hw = bounds%half_width()

    ! Note, input work arrays are allocated with upper bounds, so always explicitly specify elements
    SAFE_ALLOCATE(shifted_eigenvalues(1:n_states))
    shifted_eigenvalues(1:n_states) = (eigenvalues(1:n_states) - c) / hw

    ! First polynomial degree, n = 1
    T%n_minus_1(1:n_states) = M_ONE
    T%n(1:n_states) = shifted_eigenvalues(1:n_states)

    do n = 1, max_degree
      ! Require epsilon * |T_n(E1)| << 1 or equivalently, |T_n(E1)| << 1 / epsilon
      ! i.e. Do not want |T_n(E1)| to blow up, hence do no want |r| / |T_n(E1)| -> zero
      if ( abs(T%n(1)) > 0.01_real64 / M_EPSILON) then
        exit
      endif
      ! Do not reduce any residual by more than an order of magnitude
      if ( any(abs(T%n(1:n_states)) >= 10._real64) ) then
        exit
      endif
      ! Return if the largest predicted residual is less than the target residual precision
      if ( maxval(abs(prior_residual(1:n_states) / T%n(1:n_states))) < target_residual ) then
        exit
      endif

      if (n == max_degree) exit
      ! T_n+1 from the prior definition of T_n and T_n-1, i.e. new T_n
      T%n_plus_1(1:n_states) = M_TWO * shifted_eigenvalues(1:n_states) * T%n(1:n_states) -  T%n_minus_1(1:n_states)
      T%n_minus_1(1:n_states) = T%n(1:n_states)
      T%n(1:n_states) = T%n_plus_1(1:n_states)
    enddo

    filter_degree = n
    SAFE_DEALLOCATE_A(shifted_eigenvalues)
    POP_SUB(optimal_polynomial_degree)

  end function optimal_polynomial_degree

end module chebyshev_filter_oracle_oct_m
