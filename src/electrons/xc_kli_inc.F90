!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!! Copyright (C) 2012-2013 M. Gruning, P. Melo, M. Oliveira
!! Copyright (C) 2021 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

! ---------------------------------------------------------
!>@brief This routine solves the KLI approximation to the OEP equation.
!!
!! This implements the direct solution of the KLI equation.
subroutine X(xc_KLI_solve) (space, mesh, st, oep, rcell_volume)
  class(space_t),           intent(in)    :: space
  class(mesh_t),            intent(in)    :: mesh
  type(states_elec_t),      intent(in)    :: st
  type(xc_oep_t),           intent(inout) :: oep
  FLOAT,                    intent(in)    :: rcell_volume

  integer :: ist, ip, jst, eigen_n, kssi, kssj, ispin, ik
  FLOAT, allocatable :: rho_sigma(:,:), v_bar_S(:), sqphi(:, :, :), dd(:)
  FLOAT, allocatable :: Ma(:,:), xx(:,:), yy(:,:), kli(:,:)
  R_TYPE, allocatable :: psi(:, :)
  FLOAT :: cnst(st%d%nspin)
  FLOAT, parameter :: min_density = 1e-20_real64

  PUSH_SUB(X(xc_KLI_solve))

  call profiling_in(C_PROFILING_XC_KLI, TOSTRING(X(XC_KLI)))


  ! some intermediate quantities
  SAFE_ALLOCATE(rho_sigma(1:mesh%np, 1:st%d%spin_channels))

  do ispin = 1, st%d%spin_channels
    !omp parallel do
    do ip = 1, mesh%np
      oep%vxc(ip, ispin) = M_ZERO
    end do
  end do

  SAFE_ALLOCATE(sqphi(1:mesh%np, 1:st%nst, st%d%kpt%start:st%d%kpt%end))
  SAFE_ALLOCATE(psi(1:mesh%np, 1:st%d%dim))

  ! Add a lower bound to the density to avoid numerical issues
  do ispin = 1, st%d%spin_channels
    !$omp parallel do
    do ip = 1, mesh%np
      rho_sigma(ip, ispin) = oep%socc*max(st%rho(ip, ispin), min_density)
    end do
  end do

  ! Comparing to KLI paper 1990, oep%vxc corresponds to V_{x \sigma}^S in Eq. 8
  ! The n_{i \sigma} in Eq. 8 is partitioned in this code into \psi^{*} (included in lxc) and \psi (explicitly below)

  do ik = st%d%kpt%start, st%d%kpt%end
    ispin = st%d%get_spin_index(ik)
    do ist = st%st_start, st%st_end
      if (abs(st%occ(ist, ik)*st%kweights(ik)) < M_MIN_OCC) cycle

      call states_elec_get_state(st, mesh, ist, ik, psi)

      !$omp parallel do
      do ip = 1, mesh%np
        sqphi(ip, ist, ik) = R_REAL(R_CONJ(psi(ip, 1))*psi(ip, 1))

        oep%vxc(ip, ispin) = oep%vxc(ip, ispin) + oep%socc * st%occ(ist, ik) * st%kweights(ik) &
          * R_REAL(oep%X(lxc)(ip, 1, ist, ik)*psi(ip, 1))
      end do
    end do
  end do
  SAFE_DEALLOCATE_A(psi)

  if(st%parallel_in_states .or. st%d%kpt%parallel) then
    call comm_allreduce(st%st_kpt_mpi_grp, oep%vxc)
  end if


  do ispin = 1, st%d%spin_channels
    !$omp parallel do
    do ip = 1, mesh%np
      oep%vxc(ip, ispin) = oep%vxc(ip, ispin)/rho_sigma(ip, ispin)
    end do
  end do

  ! We have now computed the Slater part. We are adding the explicit KLI part now


  SAFE_ALLOCATE(kli(1:mesh%np, 1:st%d%nspin))
  kli = M_ZERO

  do ik = st%d%kpt%start, st%d%kpt%end
    ispin = st%d%get_spin_index(ik)

    ! In the case of isolated system, the OEP equation is simply solved for N-1 electrons
    ! whereas for solids we remove a constant, as given in PRB 93, 205205 (2016)
    if(space%is_periodic()) then
      eigen_n = st%nst
    else
      call xc_oep_AnalyzeEigen(oep, st, ik)
      eigen_n = oep%eigen_n
    end if

    SAFE_ALLOCATE(v_bar_S(1:st%nst))
    v_bar_S = M_ZERO
    do ist = st%st_start, st%st_end
      if (st%occ(ist, ik) > M_MIN_OCC) then
        v_bar_S(ist) = dmf_dotp(mesh, sqphi(:, ist, ik), oep%vxc(:, ispin), reduce = .false.)
      end if
    end do
    if (mesh%parallel_in_domains) call mesh%allreduce(v_bar_S, dim = st%st_end)

    if (st%parallel_in_states) then
      ! Broadcast the vector v_bar_S  and sqphi to all processors
      do ist = 1, st%nst
        call st%mpi_grp%bcast(v_bar_S(ist), 1, MPI_FLOAT, st%node(ist))
      end do
      do ist = 1, eigen_n
        kssi = get_state_index(ist)
        call st%mpi_grp%bcast(sqphi(1, kssi, ik), mesh%np, MPI_FLOAT, st%node(kssi))
      end do
    end if

    ! If there is more than one state, then solve linear equation.
    if (eigen_n > 0) then
      SAFE_ALLOCATE(dd(1:mesh%np))
      SAFE_ALLOCATE(xx(1:eigen_n, 1))
      SAFE_ALLOCATE(Ma(1:eigen_n, 1:eigen_n))
      SAFE_ALLOCATE(yy(1:eigen_n, 1))
      xx = M_ZERO
      yy = M_ZERO
      Ma = M_ZERO
      dd = M_ZERO

      ! We start by constructing the matrix Mij = \int n_i(r) n_j(r) / n(r)
      ! See Eq. 62 of the KLI paper PRA 45, 101 (1992)
      i_loop: do ist = 1, eigen_n
        kssi = get_state_index(ist)
        if (st%mpi_grp%rank == st%node(kssi)) then
          dd(1:mesh%np) = sqphi(1:mesh%np, kssi, ik) / rho_sigma(1:mesh%np, ispin)

          j_loop: do jst = ist, eigen_n
            kssj = get_state_index(jst)
            Ma(ist, jst) = - dmf_dotp(mesh, dd, sqphi(:, kssj, ik))
            ! See Eq. 33 in Phys. Rev. A 46, 5453 (1992)
            Ma(ist, jst) = Ma(ist, jst) * st%occ(kssj, ik) * oep%socc
          end do j_loop
          Ma(ist, ist) = M_ONE + Ma(ist, ist)

          yy(ist, 1) = v_bar_S(kssi) - oep%uxc_bar(1, kssi, ik)

        end if
      end do i_loop

#if defined(HAVE_MPI)
      if (st%parallel_in_states) then
        do ist = 1, eigen_n
          kssi = get_state_index(ist)
          call  st%mpi_grp%bcast(yy(ist, 1), 1, MPI_FLOAT, st%node(kssi))
          do jst = 1, eigen_n
            call st%mpi_grp%bcast(Ma(ist, jst), 1, MPI_FLOAT, st%node(kssi))
          end do
        end do
      end if
#endif

      ! Symmetrize the matrix
      call upper_triangular_to_hermitian(eigen_n, Ma)

      call lalg_linsyssolve(eigen_n, 1, Ma, yy, xx)

      do ist = 1, eigen_n
        kssi = get_state_index(ist)

        kli(1:mesh%np,ispin) = kli(1:mesh%np,ispin) + st%kweights(ik) * &
          oep%socc * st%occ(kssi, ik) * xx(ist, 1) * sqphi(1:mesh%np, kssi, ik) / rho_sigma(1:mesh%np, ispin)
      end do

      SAFE_DEALLOCATE_A(dd)
      SAFE_DEALLOCATE_A(xx)
      SAFE_DEALLOCATE_A(Ma)
      SAFE_DEALLOCATE_A(yy)

    end if
    ! The previous stuff is only needed if eigen_n>0.
    SAFE_DEALLOCATE_A(v_bar_S)

  end do ! ik

  if(st%d%kpt%parallel) then
    call comm_allreduce(st%d%kpt%mpi_grp, kli)
  end if

  !Adding the KLI part
  call lalg_axpy(mesh%np, st%d%nspin, M_ONE, kli, oep%vxc)
  SAFE_DEALLOCATE_A(kli)

  ! Substracting the constant, see PRB 93, 205205 (2016)
  if(space%is_periodic()) then
    SAFE_ALLOCATE(dd(1:mesh%np))
    cnst = M_ZERO
    do ik = st%d%kpt%start, st%d%kpt%end
      ispin = st%d%get_spin_index(ik)
      do ist = st%st_start, st%st_end
        if (st%occ(ist, ik) > M_MIN_OCC) then
          dd(:) = sqphi(:, ist, ik) / rho_sigma(:, ispin)
          cnst(ispin) = cnst(ispin) + oep%socc * st%occ(ist, ik) * st%kweights(ik) &
            * dmf_dotp(mesh, sqphi(:, ist, ik), oep%vxc(:,ispin)) * dmf_integrate(mesh, dd)
        end if
      end do
    end do

    if (st%parallel_in_states .or. st%d%kpt%parallel) then
      call comm_allreduce(st%st_kpt_mpi_grp, cnst)
    end if

    do ispin = 1, st%d%spin_channels
      oep%vxc(:, ispin) = oep%vxc(:, ispin) - cnst(ispin)/rcell_volume
    end do
    SAFE_DEALLOCATE_A(dd)
  end if
  SAFE_DEALLOCATE_A(xx)
  SAFE_DEALLOCATE_A(Ma)
  SAFE_DEALLOCATE_A(yy)


  SAFE_DEALLOCATE_A(rho_sigma)
  SAFE_DEALLOCATE_A(sqphi)
  call profiling_out(C_PROFILING_XC_KLI)
  POP_SUB(X(xc_KLI_solve))

contains
  integer pure function get_state_index(ist) result(kssi)
    integer, intent(in) :: ist

    if(space%is_periodic()) then
      kssi = ist
    else
      kssi = oep%eigen_index(ist)
    end if

  end function get_state_index

end subroutine X(xc_KLI_solve)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
