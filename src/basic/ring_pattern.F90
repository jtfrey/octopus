!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module is an helper to perform ring-pattern communications among all states
!!
!! This takes into account possible double-sided communication, to reduce the number of commuications
!
module ring_pattern_oct_m
  use comm_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use mpi_oct_m
  use profiling_oct_m

  implicit none

  private

  public ::                      &
    ring_pattern_t

  type ring_pattern_t
    private

    type(mpi_grp_t) :: mpi_grp
    integer :: nsteps                !< Total number of communication steps to be done for doing the full ring.
    !                                   Each step might imply more than one communication step (e.g. multiple batches per rank).
    logical :: double_sided_comms    !< Is the ring pattern performed in a back and forth manner (halving the number of communications)

  contains
    procedure :: start => ring_pattern_start
    procedure :: get_nsteps => ring_pattern_get_nsteps
    procedure :: get_rank_from => ring_pattern_get_rank_from
    procedure :: get_rank_to => ring_pattern_get_rank_to
  end type ring_pattern_t

contains

  !>@brief Starts the ring pattern scheme.
  !!
  !! Initializes the quantities properly
  !!
  !! We send to the p+s task and receive from the p-s task
  !! This follows the scheme of Cardfiff et al. J. Phys.: Condens. Matter 30 095901 (2018)
  subroutine ring_pattern_start(this, mpi_grp, double_sided_comms)
    class(ring_pattern_t),   intent(inout) :: this
    type(mpi_grp_t),         intent(in)    :: mpi_grp
    logical,                 intent(in)    :: double_sided_comms

    PUSH_SUB(ring_pattern_start)

    this%double_sided_comms = double_sided_comms
    this%mpi_grp = mpi_grp

    if (double_sided_comms) then
      this%nsteps = int((mpi_grp%size+2)/2)-1
    else
      this%nsteps = mpi_grp%size-1
    end if

    if (debug%info) then
      write(message(1), '(a,i4,a)') "The ring pattern will perform ", this%nsteps, " steps."
      call messages_info(1)
    end if

    POP_SUB(ring_pattern_start)
  end subroutine ring_pattern_start

  !>@brief Returns the total number of steps in the ring
  integer pure function ring_pattern_get_nsteps(this)
    class(ring_pattern_t), intent(in) :: this

    ring_pattern_get_nsteps = this%nsteps
  end function ring_pattern_get_nsteps

  !>@brief Returns the rank where to send the information.
  !!
  !! Returned values are from -1 to this%mpi_grp%size, where -1 means no communication.
  !!
  !! If we do double sided communications, we halve the number of communications.
  !! For odd or even, the behavior is not the same, so this check ends the communications
  !! by setting the receiving rank to -1 (ranks starts at 0), causing the loop to exit.
  !! What this means is that for certain steps, not all tasks will send or receive information.
  !! This is the scheme that works like this.
  !!
  !! The example is four tasks we get 2 steps to do
  !!
  !!  in step 1, we do 1 <-> 2, 2<->3, 3<->4, 4<->1
  !!  in step 2, we do 1<->3, 2<->4 but 3<->1 and 4<->2 are excluded.
  !!
  !! (Here <-> means the combination of sending to ->, and getting back from <- )
  !!
  !! You can show easily that for the last step, for even grid,
  !! only the first half of the tasks need to send something. This is the condition implemented here.
  integer pure function ring_pattern_get_rank_to(this, istep) result(rank_to)
    class(ring_pattern_t), intent(in) :: this
    integer,               intent(in) :: istep

    logical :: last_step, even_number_tasks, rank_to_first_half

    rank_to = mod(this%mpi_grp%rank + istep, this%mpi_grp%size)

    if (this%double_sided_comms) then
      last_step = istep == this%nsteps
      even_number_tasks = mod(this%mpi_grp%size, 2) == 0
      rank_to_first_half = rank_to < this%mpi_grp%size/2

      if (last_step .and. even_number_tasks .and. rank_to_first_half) then
        rank_to = -1
      end if
    end if
  end function ring_pattern_get_rank_to

  !>@brief Returns the rank from which we receive the information
  integer pure function ring_pattern_get_rank_from(this, istep) result(rank_fr)
    class(ring_pattern_t), intent(in) :: this
    integer,               intent(in) :: istep

    logical :: last_step, even_number_tasks, rank_first_half

    rank_fr = this%mpi_grp%rank - istep
    if (rank_fr < 0) rank_fr = this%mpi_grp%size + rank_fr
    rank_fr = mod(rank_fr, this%mpi_grp%size)

    if (this%double_sided_comms) then
      last_step = istep == this%nsteps
      even_number_tasks = mod(this%mpi_grp%size, 2) == 0
      rank_first_half = this%mpi_grp%rank < this%mpi_grp%size/2
      if (last_step .and. even_number_tasks .and. rank_first_half) then
        rank_fr = -1
      end if
    end if
  end function ring_pattern_get_rank_from

end module ring_pattern_oct_m


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
