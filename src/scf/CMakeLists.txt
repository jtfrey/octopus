target_sources(Octopus_lib PRIVATE
		criteria_factory.F90
		density_criterion.F90
		eigenval_criterion.F90
		electrons_ground_state.F90
		energy_criterion.F90
		lcao.F90
		lda_u_mixer.F90
		mix.F90
		rdmft.F90
		scf.F90
		unocc.F90
		)
## Unused sources
# mixing_metric.F90
