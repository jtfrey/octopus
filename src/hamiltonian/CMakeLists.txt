target_sources(Octopus_lib PRIVATE
		absorbing_boundaries.F90
		berry.F90
		energy.F90
		epot.F90
		exchange_operator.F90
		ext_partner_list.F90
		gauge_field.F90
		hamiltonian_abst.F90
		hamiltonian_elec.F90
		hamiltonian_elec_base.F90
		hgh_projector.F90
		hirshfeld.F90
		ion_interaction.F90
		kb_projector.F90
		kick.F90
		lasers.F90
		lda_u.F90
		lda_u_io.F90
		libvdwxc.F90
		magnetic_constrain.F90
	        mxll_elec_coupling.F90
		oct_exchange.F90
		pcm.F90
		pcm_eom.F90
		pcm_potential.F90
		projector.F90
		projector_matrix.F90
		rkb_projector.F90
		scf_tol.F90
		scissor.F90
		singularity.F90
		species_pot.F90
		vdw_ts.F90
		vdw_ts_low.c
		xc.F90
		xc_functional.F90
		xc_vdw.F90
		zora.F90
		)

## External libraries
target_link_libraries(Octopus_lib PRIVATE Libxc::xcf03)
if (TARGET libvdwxc::libvdwxc)
	target_link_libraries(Octopus_lib PRIVATE libvdwxc::libvdwxc)
endif ()
