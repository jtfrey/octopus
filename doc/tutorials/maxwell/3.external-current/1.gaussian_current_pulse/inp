# ----- Calculation mode and parallelization ------------------------------------------------------

 CalculationMode   = td
 ExperimentalFeatures = yes
 FromScratch = yes

%Systems
  'Maxwell' | maxwell
%

 Maxwell.ParDomains = auto
 Maxwell.ParStates  = no

# ----- Maxwell box variables ---------------------------------------------------------------------

 # free maxwell box limit of 10.0 plus 2.0 for the incident wave boundaries with
 # der_order = 4 times dx_mx

 lsize_mx = 10.0
 dx_mx    = 0.5

 Maxwell.BoxShape   = parallelepiped

 %Maxwell.Lsize
  lsize_mx | lsize_mx | lsize_mx
 %

 %Maxwell.Spacing
  dx_mx | dx_mx | dx_mx
 %


# ----- Maxwell calculation variables -------------------------------------------------------------

 HamiltonianOperator = faraday_ampere

 %MaxwellBoundaryConditions
  zero | zero | zero
 %

 %MaxwellAbsorbingBoundaries
  not_absorbing | not_absorbing | not_absorbing
 %

# snipppet_start output
# ----- Output variables --------------------------------------------------------------------------

 OutputFormat = axis_x + plane_x + plane_y + plane_z


# ----- Maxwell output variables ------------------------------------------------------------------

 %MaxwellOutput
  electric_field
  magnetic_field
  maxwell_energy_density
  trans_electric_field
  external_current
  div_electric_field
  div_magnetic_field
  charge_density
 %

 MaxwellOutputInterval = 1
 MaxwellTDOutput = maxwell_energy + maxwell_total_e_field
 

 %MaxwellFieldsCoordinate
   0.00 | 0.00 | 0.00
 %
# snippet_end

# ----- Time step variables -----------------------------------------------------------------------

 TDSystemPropagator = prop_expmid
 timestep                          = 1 / ( sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2) )
 TDTimeStep                        = timestep
 TDPropagationTime                 = 180 * timestep

# Maxwell field variables

# snippet_start current
# external current

ExternalCurrent = yes
t1 = (180 * timestep) / 2
tw = (180 * timestep) / 6
j = 1.0000
sigma = 0.5
lambda = 5.0
omega = 2 * pi * c / lambda

%UserDefinedMaxwellExternalCurrent
 current_td_function | "0" | "0" | "j*exp(-(x+8)^2/2/sigma^2)*exp(-(y-5)^2/2/sigma^2)*exp(-z^2/2/sigma^2)" | omega | "env_func_1"
 current_td_function | "0" |" 0" | "j*exp(-(x+8)^2/2/sigma^2)*exp(-y^2/2/sigma^2)*exp(-z^2/2/sigma^2)"     | omega | "env_func_1"
 current_td_function | "0" | "0" | "j*exp(-(x+8)^2/2/sigma^2)*exp(-(y+5)^2/2/sigma^2)*exp(-z^2/2/sigma^2)" | omega | "env_func_1"
%

%TDFunctions
 "env_func_1" | tdf_gaussian | 1.0 | tw | t1
%
# snippet_end
