set pm3d
set view map
set palette defined (-0.05 "blue", 0 "white", 0.05"red")
set term png size 1000,300

unset surface
unset key

set output 'tutorial_04.1-drude-01.png'

set xlabel 'x-direction'
set ylabel 'y-direction'
set cbrange [-0.8:0.8]

set multiplot

set origin 0.025,0
set size 0.3,0.9
set size square
set title 'Electric field E_z - step 60'
sp [-400:400][-400:400] 'Maxwell/output_iter/td.0000060/e_field-z.y=0' u ($1/18.897):($2/18.897):3

set origin 0.35,0
set size 0.3,0.9
set size square
set title 'Electric field E_z - step 120'
sp [-400:400][-400:400] 'Maxwell/output_iter/td.0000120/e_field-z.y=0' u ($1/18.897):($2/18.897):3

set origin 0.675,0
set size 0.3,0.9
set size square
set title 'Electric field E_z - step 240'
sp [-400:400][-400:400] 'Maxwell/output_iter/td.0000240/e_field-z.y=0' u ($1/18.897):($2/18.897):3
unset multiplot

