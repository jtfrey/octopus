---
Title: "Manual"
weight: 20
description: "the user manual"
menu: "top"
---

Welcome to the Octopus online manual.


* {{< versioned-link "Citing_Octopus" "How to cite Octopus" >}}

{{% children depth=1 sort="Weight" %}}
