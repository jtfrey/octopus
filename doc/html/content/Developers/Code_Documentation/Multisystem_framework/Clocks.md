---
Title: "Clocks"
Weight: 4
---

{{< notice warning >}}
Work in progress!
{{< /notice >}}


Clocks are essential to keep things synchronized. This is also the case in {{<octopus>}}, where instances of the {{< code iteration_counter_t >}} type are used to ensure that different systems stay in sync during time propagation ot iterations. Clocks are used to determine whether quantities or interactions need to be updated in a propagation step.

The smallest unit of time in {{< octopus >}} is one {{< code tick >}}.


{{% expand "Definition of iteration_counter_t" %}}
```Fortran
#include_type_def iteration_counter_t
```
{{% /expand %}}

See also 
* [{{<code "clock_oct_m">}}](https://octopus-code.org/doc/main/doxygen_doc/namespaceclock__oct__m.html)
* [{{<code "iteration_counter_t">}}](https://octopus-code.org/doc/main/doxygen_doc/structiteration__counter__oct__m_1_1iteration__counter__t.html)