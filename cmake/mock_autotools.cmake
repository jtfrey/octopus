# #639 TODO: When removing autotools, replace flags with target_compile_define

list(APPEND CMAKE_MESSAGE_CONTEXT mock_autotools)
string(TIMESTAMP BUILD_TIME)
execute_process(
		COMMAND git log -1 --format=%h
		WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
		OUTPUT_VARIABLE GIT_HASH
		OUTPUT_STRIP_TRAILING_WHITESPACE
)

if (CMAKE_BUILD_TYPE MATCHES Debug.*)
	set(SHARE_DIR ${PROJECT_BINARY_DIR}/share)
else ()
	set(SHARE_DIR ${CMAKE_INSTALL_FULL_DATAROOTDIR}/octopus)
endif ()

if (MPI_Fortran_FOUND)
	set(MPI_MOD 1)
	set(HAVE_MPI 1)
endif ()
if (TARGET FFTW::Double OR TARGET MKL::MKL)
	set(HAVE_FFTW3 1)
	if (OCTOPUS_OpenMP AND (TARGET FFTW::DoubleOpenMP OR TARGET FFTW::DoubleThreads)
			OR (TARGET MKL::MKL AND MKL_THREADING AND NOT MKL_THREADING STREQUAL sequential))
		set(HAVE_FFTW3_THREADS 1)
	endif ()
endif ()
if (OCTOPUS_ScaLAPACK)
	set(HAVE_SCALAPACK 1)
endif ()

# Currently long lines have to be supported to avoid preprocess.pl
if (CMAKE_Fortran_COMPILER_ID MATCHES GNU)
	set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -ffree-line-length-none")
endif ()
set(LONG_LINES 1)

try_compile(FC_Test_SUCCESS ${CMAKE_CURRENT_BINARY_DIR}
		SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/cmake/test_compiler.F90
		OUTPUT_VARIABLE FC_Test_OUTPUT)
if (NOT FC_Test_SUCCESS)
	message(SEND_ERROR "Failed to build test fortran file cmake/test_compiler.F90 with:
	CMAKE_Fortran_COMPILER: ${CMAKE_Fortran_COMPILER}
	CMAKE_Fortran_FLAGS: ${CMAKE_Fortran_FLAGS}")
	message(FATAL_ERROR ${FC_Test_OUTPUT})
endif ()
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
