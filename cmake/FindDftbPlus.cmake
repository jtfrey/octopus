#[==============================================================================================[
#                                DftbPlus compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# FindDftbPlus

DftbPlus compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET DftbPlus::DftbPlus)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindDftbPlus)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES DftbPlus dftbplus
        PKG_MODULE_NAMES dftbplus)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(DftbPlus::DftbPlus ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_DFTBPLUS 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/dftbplus/dftbplus
        DESCRIPTION "DFTB+ general package for performing fast atomistic simulations "
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
