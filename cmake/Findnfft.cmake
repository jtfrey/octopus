#[==============================================================================================[
#                                  nfft compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# Findnfft

nfft compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET nfft::nfft)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findnfft)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES nfft
        PKG_MODULE_NAMES nfft3)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(nfft::nfft ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_NFFT 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/NFFT/nfft
        DESCRIPTION "NFFT is a software library, written in C, for computing non-equispaced fast Fourier transforms and related variations"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
