# -*- coding: utf-8 mode: shell-script -*-

Test       : Band structure calculation
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Processors : 4

Input      : 15-bandstructure.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 64
match ; Reduced k-points ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 64
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 227
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;  24

Precision: 3.96e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -7.92827696
Precision: 3.93e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -7.8578008
Precision: 1.27e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.25349502
Precision: 2.75e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.54939382
Precision: 1.02e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -2.0344421400000003
Precision: 1.88e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.37502847
Precision: 1.54e-07
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 3.07996561
Precision: 1.29e-14
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -1.29036516

Precision: 7.73e-06
match ;   DOS E Fermi      ; LINEFIELD(static/total-dos-efermi.dat, 2, 1) ; 0.154622
Precision: 1.99e-05
match ;   DOS energy 2     ; LINEFIELD(static/total-dos.dat, 2, 1) ; -0.397809
Precision: 6.07e-06
match ;   DOS value  2     ; LINEFIELD(static/total-dos.dat, 2, 2) ; 0.121431
Precision: 9.34e-06
match ;   DOS energy 442   ; LINEFIELD(static/total-dos.dat, 442, 1) ; 0.186727
Precision: 3.34e-05
match ;   DOS value  442   ; LINEFIELD(static/total-dos.dat, 442, 2) ; 0.667236

Precision: 6.00e-01
match ;   Angle alpha         ; GREPFIELD(out, 'alpha =', 3) ; 60.0
match ;   Angle beta          ; GREPFIELD(out, 'beta  =', 3) ; 60.0
match ;   Angle gamma         ; GREPFIELD(out, 'gamma =', 3) ; 60.0

Precision: 3.76e-05
match ;   PDOS energy 1 tot   ; LINEFIELD(static/pdos-at001-Si3p.dat, 2, 2) ; 0.075116
Precision: 1.25e-05
match ;   PDOS energy 1 py    ; LINEFIELD(static/pdos-at001-Si3p.dat, 2, 3) ; 0.025039
Precision: 1.25e-05
match ;   PDOS energy 1 pz    ; LINEFIELD(static/pdos-at001-Si3p.dat, 2, 4) ; 0.025039
Precision: 1.25e-05
match ;   PDOS energy 1 px    ; LINEFIELD(static/pdos-at001-Si3p.dat, 2, 5) ; 0.025039

Precision: 1.44e-05
match ;   Eigenvalue  1   ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.287323
Precision: 7.73e-06
match ;   Eigenvalue  4   ; GREPFIELD(static/info, '#k =       1', 3, 4) ; 0.154622
Precision: 1.29e-05
match ;   Eigenvalue  1   ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -0.257446
Precision: 6.32e-06
match ;   Eigenvalue  4   ; GREPFIELD(static/info, '#k =       2', 3, 4) ; 0.126427

Input      : 15-bandstructure.02-unocc.inp

Precision: 1.00e-04
match ;   Red. coord. k1   ; LINEFIELD(static/bandstructure, 2, 1) ; 0.0
Precision: 5.00e-03
match ;   Band structure k1x   ; LINEFIELD(static/bandstructure, 2, 2) ; 0.5
Precision: 1.00e-04
match ;   Band structure k1y   ; LINEFIELD(static/bandstructure, 2, 3) ; 0.0
Precision: 1.00e-04
match ;   Band structure k1z   ; LINEFIELD(static/bandstructure, 2, 4) ; 0.0
Precision: 9.97e-08
match ;   Band structure E1(k1)   ; LINEFIELD(static/bandstructure, 2, 5) ; -0.19943704
Precision: 5.22e-08
match ;   Band structure E2(k1)   ; LINEFIELD(static/bandstructure, 2, 6) ; -0.10447787
Precision: 5.51e-08
match ;   Band structure E3(k1)   ; LINEFIELD(static/bandstructure, 2, 7) ; 0.11027722
Precision: 6.60e-08
match ;   Red. coord. k3   ; LINEFIELD(static/bandstructure, 4, 1) ; 0.13200645
Precision: 2.50e-03
match ;   Band structure k3x   ; LINEFIELD(static/bandstructure, 4, 2) ; 0.25
Precision: 1.00e-04
match ;   Band structure k3y   ; LINEFIELD(static/bandstructure, 4, 3) ; 0.0
Precision: 1.00e-04
match ;   Band structure k3z   ; LINEFIELD(static/bandstructure, 4, 4) ; 0.0
Precision: 1.41e-07
match ;   Band structure E7(k3)   ; LINEFIELD(static/bandstructure, 4, 11) ; 0.28296292
Precision: 2.04e-07
match ;   Band structure E8(k3)   ; LINEFIELD(static/bandstructure, 4, 12) ; 0.40896074
Precision: 2.09e-07
match ;   Band structure E9(k3)   ; LINEFIELD(static/bandstructure, 4, 13) ; 0.41858865
Precision: 1.70e-07
match ;   Red. coord. k7   ; LINEFIELD(static/bandstructure, 7, 1) ; 0.34022686
Precision: 1.00e-04
match ;   Band structure k7x   ; LINEFIELD(static/bandstructure, 7, 2) ; 0.0
Precision: 6.25e-03
match ;   Band structure k7y   ; LINEFIELD(static/bandstructure, 7, 3) ; 0.125
Precision: 6.25e-03
match ;   Band structure k7z   ; LINEFIELD(static/bandstructure, 7, 4) ; 0.125
Precision: 1.45e-07
match ;   Band structure E7(k7)   ; LINEFIELD(static/bandstructure, 7, 11) ; 0.29076089
Precision: 1.59e-07
match ;   Band structure E8(k7)   ; LINEFIELD(static/bandstructure, 7, 12) ; 0.31866913
Precision: 1.97e-07
match ;   Band structure E9(k7)   ; LINEFIELD(static/bandstructure, 7, 13) ; 0.39316426

MPIUtil    : oct-wannier90
Input      : 15-bandstructure.03-wannier90_setup.inp
match ; Unit cell rlattice 1,1 ; GREPFIELD(w90.win, 'begin unit_cell_cart', 1, 2) ; 0.00000000
match ; Unit cell rlattice 2,1 ; GREPFIELD(w90.win, 'begin unit_cell_cart', 2, 2) ; 2.69880376
match ; Unit cell rlattice 3,1 ; GREPFIELD(w90.win, 'begin unit_cell_cart', 3, 2) ; 2.69880376
match ; Atom 1 coord. 1 ; GREPFIELD(w90.win, 'begin atoms_frac', 2, 1) ; 0.00000000
match ; Atom 2 coord. 1 ; GREPFIELD(w90.win, 'begin atoms_frac', 2, 2) ; 0.25000000
match ; num_bands ; GREPFIELD(w90.win, 'num_bands', 2) ; 4
match ; num_wann ; GREPFIELD(w90.win, 'num_wann', 2) ; 4
match ; mp_grid 1 ; GREPFIELD(w90.win, 'mp_grid', 2) ; 4
match ; mp_grid 2 ; GREPFIELD(w90.win, 'mp_grid', 3) ; 4
match ; mp_grid 3 ; GREPFIELD(w90.win, 'mp_grid', 4) ; 4
match ; k-point  1 (x) ; GREPFIELD(w90.win, 'begin kpoints', 1, 1) ; 0.000000
match ; k-point  1 (y) ; GREPFIELD(w90.win, 'begin kpoints', 2, 1) ; 0.000000
match ; k-point  1 (z) ; GREPFIELD(w90.win, 'begin kpoints', 3, 1) ; 0.000000
match ; k-point 10 (x) ; GREPFIELD(w90.win, 'begin kpoints', 1, 10) ; -0.25000000
match ; k-point 10 (y) ; GREPFIELD(w90.win, 'begin kpoints', 2, 10) ;  0.25000000
match ; k-point 10 (z) ; GREPFIELD(w90.win, 'begin kpoints', 3, 10) ;  0.00000000
match ; k-point 20 (x) ; GREPFIELD(w90.win, 'begin kpoints', 1, 20) ; -0.25000000
match ; k-point 20 (y) ; GREPFIELD(w90.win, 'begin kpoints', 2, 20) ; -0.25000000
match ; k-point 20 (z) ; GREPFIELD(w90.win, 'begin kpoints', 3, 20) ; -0.25000000


MPIUtil    : oct-wannier90
ExtraFile  : 15-bandstructure.04-wannier90.win
ExtraFile  : 15-bandstructure.04-wannier90.nnkp
Input      : 15-bandstructure.04-wannier90_output.inp

Precision: 3.91e-04
match ;   w90.eig 1   ; LINEFIELD(15-bandstructure.04-wannier90.eig, 1, 3) ; -7.81844
Precision: 2.10e-04
match ;   w90.eig 2   ; LINEFIELD(15-bandstructure.04-wannier90.eig, 2, 3) ; 4.20747
Precision: 2.10e-04
match ;   w90.eig 3   ; LINEFIELD(15-bandstructure.04-wannier90.eig, 3, 3) ; 4.20747
Precision: 2.10e-14
match ;   w90.amn 4-5 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.amn, -4, 4, 5) ; 1.5384600000000002e-11
Precision: 1.51e-02
match ;   w90.amn 4-6 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.amn, -3, 4, 5) ; 0.265024
Precision: 1.71e-04
match ;   w90.amn 4-7 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.amn, -2, 4, 5) ; 0.526467
Precision: 1.52e-02
match ;   w90.amn 4-8 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.amn, -1, 4, 5) ; 0.26135
Precision: 4.18e-14
match ;   w90.mmn 4-1 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.mmn, -4, 1, 2) ; 1.65537e-11
Precision: 2.01e-13
match ;   w90.mmn 4-2 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.mmn, -3, 1, 2) ; 4.7022e-12
Precision: 2.06e-02
match ;   w90.mmn 4-3 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.mmn, -2, 1, 2) ; 0.022197900000000003
Precision: 4.69e-04
match ;   w90.mmn 4-4 kpt64  ; LINEFIELD_ABS(15-bandstructure.04-wannier90.mmn, -1, 1, 2) ; 0.975148

MPIUtil    : oct-wannier90
ExtraFile  : 15-bandstructure.04-wannier90.win
ExtraFile  : 15-bandstructure.04-wannier90.nnkp
ExtraFile  : 15-bandstructure.04-wannier90_centres.xyz
ExtraFile  : 15-bandstructure.04-wannier90_u.mat
Input      : 15-bandstructure.05-wannier90_states.inp

match ; 1st Wannier function ; LINEFIELD(wannier/wannier-001.x\=0, 87, 2) ; -7.28571428571429E-001
