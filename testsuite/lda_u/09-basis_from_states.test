# -*- coding: utf-8 mode: shell-script -*-

Test       : Localized basis from LDA states and ACBN0 functional
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes

ExtraFile: 09-basis_from_states.fhi

Input      : 09-basis_from_states.01-lda.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 1.96e-07
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -39.28513107
Precision: 1.12e-07
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 3.29e-07
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -6.586052850000001
Precision: 6.06e-08
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.12934033
Precision: 2.78e-07
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.56821768
Precision: 2.83e-07
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56648564
Precision: 3.66e-13
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.63344599
Precision: 2.97e-07
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -59.41802846

Precision: 1.00e-04
match ;     k-point 1 (x)     ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;     k-point 1 (y)     ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;     k-point 1 (z)     ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 5.54e-06
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.107092
Precision: 5.54e-06
match ;     Eigenvalue  2     ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.107092
Precision: 1.35e-05
match ;     Eigenvalue  4     ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270262
Precision: 2.99e-14
match ;     Eigenvalue  5     ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597026

Input      : 09-basis_from_states.02-acbn0.inp
Precision: 8e-5
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 1.96e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -39.28404975
Precision: 1.12e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 4.43e-07
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -8.85242623
Precision: 6.07e-08
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.13221216
Precision: 2.78e-07
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.5687416800000005
Precision: 2.83e-07
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56650456
Precision: 1.83e-07
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.63811398
Precision: 2.97e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -59.42499458
Precision: 5.25e-08
match ;    Hubbard energy        ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.00105083

Precision: 3.79e-05
match ;    U states     ; LINEFIELD(static/effectiveU, 3, 3) ; 0.757418
Precision: 1.00e-07
match ;    Occupation Ni2 up-down 3d4    ; LINEFIELD(static/occ_matrices, -2, 2) ; 1.9990745200000002
match ;    Occupation Ni2 up-down 3d5    ; LINEFIELD(static/occ_matrices, -1, 3) ; 1.99907432

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 7.42e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.48483
Precision: 7.42e-05
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.48483
Precision: 1.35e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270246
Precision: 2.99e-05
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597045

Input: 09-basis_from_states.03-intersite.inp
Precision: 8e-5
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 1

Precision: 2.09e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -41.758324079999994
Precision: 1.12e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -22.49518607
Precision: 6.90e-08
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -13.79824204
Precision: 6.07e-07
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.135606899999999
Precision: 2.78e-07
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -5.56935077
Precision: 2.83e-07
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.56652686
Precision: 3.66e-13
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 36.643673820000004
Precision: 2.97e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -59.43313511
Precision: 1.24e-06
match ;    Hubbard energy        ; GREPFIELD(static/info, 'Hubbard     =', 3) ; -2.4734054

Precision: 3.79e-05
match ;    U states     ; LINEFIELD(static/effectiveU, 3, 3) ; 0.757479
Precision: 3.44e-05
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 3) ; 0.068832
Precision: 3.44e-05
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 4) ; 0.0688
Precision: 3.44e-05
match ;    V states     ; GREPFIELD(static/info, 'Effective intersite V', 5, 5) ; 0.068888

Precision: 1.00e-07
match ;    Occupation Ni2 up-down 3d4    ; LINEFIELD(static/occ_matrices, -2, 2) ; 1.99915522
match ;    Occupation Ni2 up-down 3d5    ; LINEFIELD(static/occ_matrices, -1, 3) ; 1.99915504

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.15e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -2.309145
Precision: 1.15e-05
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -2.3091429999999997
Precision: 1.35e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -0.270225
Precision: 2.99e-05
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; 0.597069
