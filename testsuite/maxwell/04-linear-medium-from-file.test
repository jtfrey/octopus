# -*- coding: utf-8 mode: shell-script -*-

Test       : Free Maxwell propagation through a linear medium (defined in a file)
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : no-GPU


# Cosinoidal pulse polarized in z-direction passing through medium box read from file
Processors : 2
ExtraFile  : 04-linear-medium-from-file.01-cube.off
Input      : 04-linear-medium-from-file.01-cube_medium_from_file.inp

if(available cgal); then
match ;  medium_points             ; GREPFIELD(Medium/log, 'Number of points inside medium (normal coordinates):', 8) ; 9261.0

Precision: 2.81e-13
match ;     Tot. Maxwell energy [step 10]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 27.529711082278702
Precision: 1.58e-12
match ;      Tot. Maxwell energy [step 30]      ; LINEFIELD(Maxwell/td.general/maxwell_energy, 36, 3) ; 157.7142640726948

Precision: 1.0e-20
match ;   Vacuum relative permittivity   ; LINEFIELD(Medium/static/medium-permittivity\.z=0, 20, 3) ; 1.0
match ;   Medium relative permittivity   ; LINEFIELD(Medium/static/medium-permittivity\.z=0, 1540, 3) ; 2.0
match ;   Vacuum relative permeability   ; LINEFIELD(Medium/static/medium-permeability\.y=0\,z=0, 10, 2) ; 1.0
match ;   Medium relative permeability   ; LINEFIELD(Medium/static/medium-permeability\.y=0\,z=0, 30, 2) ; 2.0
match ;   Point outside medium           ; LINEFIELD(Medium/static/medium-points\.x=0\,z=0, 14, 2) ; 0.0
match ;   Point inside medium            ; LINEFIELD(Medium/static/medium-points\.x=0\,z=0, 28, 2) ; 1.0

Precision: 6.66e-18
match ;     Ex  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 20, 2) ; 0.0003465906058743775
Precision: 7.16e-18
match ;     Ex  (x=  0,y=  0,z= 10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 40, 2) ; -0.0003465906058743765
Precision: 1.00e-20
match ;     Ex  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 30, 2) ; 0.0
Precision: 3.08e-18
match ;     Ez  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 20, 2) ; 2.82567513967516e-05
Precision: 6.09e-15
match ;     Ez  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 20, 2) ; -0.121820135057744
Precision: 6.72e-20
match ;     Ez  (x= 10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 40, 2) ; -1.34479365223298e-06
Precision: 8.04e-20
match ;     Ez  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 30, 2) ; 5.8565288094115996e-08
Precision: 1.47e-27
match ;     Ez  (x= 10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 40, 2) ; -4.5984831886564346e-17
Precision: 1.78e-22
match ;     Ez  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 30, 2) ; -3.029375864688405e-11
Precision: 7.22e-18
match ;     Ez  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 20, 2) ; 0.000144446688444068
Precision: 3.38e-22
match ;     Ez  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 20, 2) ; 3.0012859140924605e-10
Precision: 1.78e-22
match ;     Ez  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 30, 2) ; -3.029375864688405e-11
Precision: 2.81e-18
match ;     Ez  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,z=0, 20, 2) ; -0.00028072728872693097
Precision: 3.12e-22
match ;     Ez  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 20, 2) ; -3.6088023768496345e-10
Precision: 1.78e-22
match ;     Ez  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 30, 2) ; -3.029375864688405e-11
Precision: 8.89e-16
match ;     By  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 20, 2) ; 0.0017779289435376
Precision: 6.59e-22
match ;     By  (x= 10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 40, 2) ; 1.9626862198939296e-08
Precision: 1.21e-21
match ;     By  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 30, 2) ; -8.547458872934151e-10
Precision: 3.46e-20
match ;     By  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 20, 2) ; -4.123990625721685e-07
Precision: 2.11e-29
match ;     By  (x= 10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 40, 2) ; 6.728244640542015e-19
Precision: 2.64e-24
match ;     By  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 30, 2) ; 4.42189597434064e-13
Precision: 2.36e-19
match ;     Bx  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 20, 2) ; -5.058387671437905e-06
Precision: 1.48e-23
match ;      Bx  (x=  0,y=-10,z=  0) [step 10]      ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 20, 2) ; 1.716577409797195e-11
Precision: 1.37e-23
match ;      Bx  (x=  0,y= 10,z=  0) [step 10]      ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 40, 2) ; -1.71657740979764e-11
Precision: 1.00e-25
match ;     Bx  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 30, 2) ; 0.0
Precision: 1.00e-22
match ;     Bx  (x=  0,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 30, 2) ; 0.0
Precision: 2.55e-24
match ;     By  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 20, 2) ; -4.37988407207871e-12
Precision: 2.64e-24
match ;     By  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 30, 2) ; 4.42189597434064e-13
Precision: 1.05e-19
match ;     By  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 20, 2) ; -2.1081567687170603e-06
Precision: 9.32e-24
match ;     By  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 20, 2) ; 5.26784148816457e-12
Precision: 2.05e-19
match ;     By  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 20, 2) ; 4.0971317202611004e-06
Precision: 2.64e-24
match ;     By  (x=  0,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 30, 2) ; 4.42189597434064e-13

else
  match ; Error cgal_not_linked        ; GREPCOUNT(err, 'CGAL'); 1
endif
