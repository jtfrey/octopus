# -*- coding: utf-8 mode: shell-script -*-

Test         : Hartree pfft
Program      : octopus
TestGroups   : short-run, components
Enabled      : Yes

# Tests and values are copied over from the 05-hartree_3d_fft tests, please keep them in sync so we test against the same values!

Input: 10-hartree_pfft.01-fft.inp
if (available pfft); then
    Precision: 2.21e-12
    match ;  Difference Hartree potential  ; GREPFIELD(hartree_results, 'Hartree test (abs.) =', 5) ; 0.4426524565815
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif

Input: 10-hartree_pfft.02-fft_corrected.inp
if (available pfft); then
    Precision: 3.33e-02
    match ;  Difference Hartree potential  ; GREPFIELD(hartree_results, 'Hartree test (abs.) =', 5) ; 0.4426524565815
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif

Input: 10-hartree_pfft.03-3d_1d_periodic.inp
if (available pfft); then
    Precision: 1.77e-12
    match ;  Difference Hartree potential  ; GREPFIELD(hartree_results, 'Hartree test (abs.) =', 5) ; 0.0353828957485
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif

Input: 10-hartree_pfft.04-3d_3d_periodic.inp
if (available pfft); then
    Precision: 1.75e-12
    match ;  Hartree energy (numerical)   ; GREPFIELD(hartree_results, 'Hartree Energy (numerical) =', 5) ; 0.3497836148185
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif

Input: 10-hartree_pfft.05-3d_2d_periodic.inp
if (available pfft); then
    Precision: 1.94e-12
    match ;  Hartree energy (numerical)   ; GREPFIELD(hartree_results, 'Hartree Energy (numerical) =', 5) ; 0.3871004614453
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif

# this is the same test as the first one, but with states parallelization only; this tests a different path in mesh2cube
Input: 10-hartree_pfft.06-fft-parstates.inp
if (available pfft); then
  Precision: 2.21e-12
  match ;  Difference Hartree potential  ; GREPFIELD(hartree_results, 'Hartree test (abs.) =', 5) ; 0.4426524565815
else
    match; Error PFFT missing; GREPCOUNT(err, 'PFFT'); 1
endif
