foreach (test IN ITEMS
		01-propagators
		02-propagators
		03-td_self_consistent
		05-time_propagation
		06-caetrs
		08-laser
		09-angular_momentum
		10-bomd
		11-tdmagnetic
		12-absorption
		13-absorption-spin
		14-absorption-spinors
		15-crank_nicolson
		16-sparskit
		17-absorption-spin_symmetry
		18-hhg
		19-td_move_ions
		20-pcm-local-field-absorption
		21-scissor
		22-td_move_ions_periodic
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
